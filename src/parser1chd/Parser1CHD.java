/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package parser1chd;

import java.io.*;
import java.util.Arrays;

/**
 *
 * @author vdementev
 */
public class Parser1CHD {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // определяем объект для каталога

        File dir;
        File dir_check;

        if (args.length > 0) {

            dir = new File(args[0]);
            dir_check = new File(args[1]);
            System.out.println("Source: " + dir.toString());
            System.out.println("Destination: " + dir_check.toString());

        } else {
            dir = new File("D:\\Work\\Java\\Parser1CHD\\1");
            dir_check = new File("D:\\Work\\Java\\Parser1CHD\\1\\check");
        }
        // если объект представляет каталог
        if (dir.isDirectory()) {
            // получаем все вложенные объекты в каталоге
            for (File item : dir.listFiles()) {

                if (item.isDirectory()) {

                    //     System.out.println(item.getName() + "  \t folder");
                } else {

                    //     System.out.println(dir.getPath() +"\\"+ item.getName() + "\t file");
                    try {

                        FileInputStream fstream = new FileInputStream(dir.getPath() + "/" + item.getName());
                        FileOutputStream wrstream = new FileOutputStream(dir_check.getPath() + "/" + item.getName() + "_check");
                        BufferedReader br = new BufferedReader(new InputStreamReader(fstream, "UTF-8"));

                        String strLine;
                        String res;

                        while ((strLine = br.readLine()) != null) {
                            
                            if ((strLine.lastIndexOf("vnutrdoc") > 0)) {
                                System.out.println(item.getName());
                                System.out.print("vnutrdoc... ");

                                int ind1 = strLine.lastIndexOf("%ДОКУМЕНТ");                           
                                String document = strLine.substring(12, ind1 - 8);

                                int ind2 = strLine.lastIndexOf("vnutrdoc");
                                String doc2 = strLine.substring(ind1, ind2);

                                int ind3 = strLine.lastIndexOf("ДАТА ");
                                String doc3 = System.lineSeparator() + strLine.substring(ind2, ind3);

                                int ind4 = strLine.lastIndexOf("ПЛАН");
                                String doc4 = System.lineSeparator() + strLine.substring(ind3, ind4);

                                int ind5 = strLine.lastIndexOf("ФИЛИАЛ");
                                String doc5 = System.lineSeparator() + strLine.substring(ind4, ind5);

                                int ind6 = strLine.lastIndexOf("ОБЛАСТЬ");
                                String doc6 = System.lineSeparator() + strLine.substring(ind5, ind6);

                                int ind7 = strLine.lastIndexOf("ВАЛЮТА ");
                                String doc7 = System.lineSeparator() + strLine.substring(ind6, ind7);

                                int ind8 = strLine.lastIndexOf("ВАЛЮТАКРЕ ");
                                String doc8 = System.lineSeparator() + strLine.substring(ind7, ind8);

                                int ind9 = strLine.lastIndexOf("ОПЕРАЦИЯ ");
                                String doc9 = System.lineSeparator() + strLine.substring(ind8, ind9);

                                int ind10 = strLine.lastIndexOf("НОМЕР");
                                String doc10 = System.lineSeparator() + strLine.substring(ind9, ind10);

                                int ind11 = strLine.lastIndexOf("НОМЕР");
                                String doc11 = "";

                                int ind12 = strLine.lastIndexOf("СУММА     :");
                                String doc12 = System.lineSeparator() + strLine.substring(ind11, ind12);

                                int ind13 = strLine.lastIndexOf("СУММАКРЕ  :");
                                String doc13 = System.lineSeparator() + strLine.substring(ind12, ind13);

                                int ind14 = strLine.lastIndexOf("СУММАПРИВ ");
                                String doc14 = System.lineSeparator() + strLine.substring(ind13, ind14);

                                int ind15 = strLine.lastIndexOf("ДАТАПЛАТ ");
                                String doc15 = System.lineSeparator() + strLine.substring(ind14, ind15);

                                int ind16 = strLine.lastIndexOf("ДЕБЕТ ");
                                String doc16 = System.lineSeparator() + strLine.substring(ind15, ind16);

                                int ind17 = strLine.lastIndexOf("КРЕДИТ ");
                                String doc17 = System.lineSeparator() + strLine.substring(ind16, ind17);

                                int ind18 = strLine.lastIndexOf("ИННОТПР ");
                                String doc18 = System.lineSeparator() + strLine.substring(ind17, ind18);

                                int ind19 = strLine.lastIndexOf("ИННПОЛУЧ ");
                                String doc19 = System.lineSeparator() + strLine.substring(ind18, ind19);

                                int ind20 = strLine.lastIndexOf("КПППЛАТ ");
                                String doc20 = System.lineSeparator() + strLine.substring(ind19, ind20);

                                int ind21 = strLine.lastIndexOf("КПППОЛ ");
                                String doc21 = System.lineSeparator() + strLine.substring(ind20, ind21);

                                int ind22 = strLine.lastIndexOf("ДАТАСП ");
                                String doc22 = System.lineSeparator() + strLine.substring(ind21, ind22);

                                int ind23 = strLine.lastIndexOf("ПРИМ1 ");
                                String doc23 = System.lineSeparator() + strLine.substring(ind22, ind23);

                                int ind24 = strLine.lastIndexOf("ПРИМ2 ");
                                String doc24 = System.lineSeparator() + strLine.substring(ind23, ind24);

                                int ind25 = strLine.lastIndexOf("ПРИМ3 ");
                                String doc25 = System.lineSeparator() + strLine.substring(ind24, ind25);

                                int ind26 = strLine.lastIndexOf("ПРИМ4 ");
                                String doc26 = System.lineSeparator() + strLine.substring(ind25, ind26);

                                int ind27 = strLine.lastIndexOf("ПРИМ5 ");
                                String doc27 = System.lineSeparator() + strLine.substring(ind26, ind27);

                                int ind28 = strLine.lastIndexOf("ПРИМ6 ");
                                String doc28 = System.lineSeparator() + strLine.substring(ind27, ind28);

                                int ind29 = strLine.lastIndexOf("ПРИМ7 ");
                                String doc29 = System.lineSeparator() + strLine.substring(ind28, ind29);

                                int ind30 = strLine.lastIndexOf("ПРИМ8 ");
                                String doc30 = System.lineSeparator() + strLine.substring(ind29, ind30);

                                int ind31 = strLine.lastIndexOf("ПРИМ9 ");
                                String doc31 = System.lineSeparator() + strLine.substring(ind30, ind31);

                                int ind32 = strLine.lastIndexOf("ПРИМ10 ");
                                String doc32 = System.lineSeparator() + strLine.substring(ind31, ind32);

                                int ind33 = strLine.lastIndexOf("ПРИМ11");
                                String doc33 = System.lineSeparator() + strLine.substring(ind32, ind33);

                                int ind34 = strLine.lastIndexOf("ПРИМ12");
                                String doc34 = System.lineSeparator() + strLine.substring(ind33, ind34);

                                int ind35 = strLine.lastIndexOf("ПРИМ13");
                                String doc35 = System.lineSeparator() + strLine.substring(ind34, ind35);

                                int ind36 = strLine.lastIndexOf("ПРИМ14 ");
                                String doc36 = System.lineSeparator() + strLine.substring(ind35, ind36);

                                int ind37 = strLine.lastIndexOf("ПРИМ15 ");
                                String doc37 = System.lineSeparator() + strLine.substring(ind36, ind37);

                                int ind38 = strLine.lastIndexOf("ПРИМ16 ");
                                String doc38 = System.lineSeparator() + strLine.substring(ind37, ind38);

                                int ind39 = strLine.lastIndexOf("ПАЧКА ");
                                String doc39 = System.lineSeparator() + strLine.substring(ind38, ind39);

                                int ind40 = strLine.lastIndexOf("УИН ");
                                String doc40 = System.lineSeparator() + strLine.substring(ind39, ind40);

                                int ind41 = strLine.lastIndexOf("%%МОПК1");
                                String doc41 = System.lineSeparator() + strLine.substring(ind40, ind41);

                                int ind42 = strLine.lastIndexOf("%%ID_ER ");
                                String doc42 = System.lineSeparator() + strLine.substring(ind41, ind42);

                                int ind43 = strLine.lastIndexOf("%END ");
                                String doc43 = System.lineSeparator() + strLine.substring(ind42, ind43);

                                res = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:onec=\"http://OneCFlowsLibrary/ru/neoflex/intesa/onecflows/intf/OneCInboundInterface\">\n"
                                        + "   <soapenv:Header/>\n"
                                        + "   <soapenv:Body>\n"
                                        + "      <onec:loadDocument>\n"
                                        + "         <requestId>" + document + "</requestId>\n"
                                        + "         <body>" + doc2 + doc3 + doc4 + doc5 + doc6 + doc7 + doc8 + doc9
                                        + doc10 + doc12 + doc13 + doc14 + doc15 + doc16 + doc17 + doc18 + doc19
                                        + doc20 + doc21 + doc22 + doc23 + doc24 + doc25 + doc26 + doc27 + doc28 + doc29
                                        + doc30 + doc31 + doc32 + doc33 + doc34 + doc35 + doc36 + doc37 + doc38 + doc39
                                        + doc40 + doc41 + doc42 + doc43 + System.lineSeparator() + "%END"
                                        + "</body>\n"
                                        + "      </onec:loadDocument>\n"
                                        + "   </soapenv:Body>\n"
                                        + "</soapenv:Envelope>";

                                //   String ress = res;
                                // System.out.println("***************");
                               
                                wrstream.write(res.getBytes("UTF-8"), 0, res.getBytes("UTF-8").length);
                                System.out.println("OK");
                                fstream.close();
                                File file = new File(dir.getPath() + "/" + item.getName());
                                if (file.delete()) {
                                    System.out.println(dir.getPath() + "/" + item.getName() +" cleared");
                                } else {
                                    System.out.println(dir.getPath() + "/" + item.getName() + " file not cleared");
                                }
                            }

                            if (strLine.lastIndexOf("mezhbank") > 0) {
                                System.out.println(item.getName());
                                System.out.print("mezhbank... ");

                                int ind1 = strLine.lastIndexOf("%МЕЖБНКДОК");
                                String document = strLine.substring(12, ind1 - 8);

                                int ind2 = strLine.lastIndexOf("mezhbank");
                                String doc2 = strLine.substring(ind1, ind2);

                                int ind3 = strLine.lastIndexOf("ДАТА ");
                                String doc3 = System.lineSeparator() + strLine.substring(ind2, ind3);

                                int ind4 = strLine.lastIndexOf("ПЛАН ");
                                String doc4 = System.lineSeparator() + strLine.substring(ind3, ind4);

                                int ind5 = strLine.lastIndexOf("ФИЛИАЛ    ");
                                String doc5 = System.lineSeparator() + strLine.substring(ind4, ind5);

                                int ind6 = strLine.lastIndexOf("ОБЛАСТЬ   ");
                                String doc6 = System.lineSeparator() + strLine.substring(ind5, ind6);

                                int ind7 = strLine.lastIndexOf("ПАЧКА ");
                                String doc7 = System.lineSeparator() + strLine.substring(ind6, ind7);

                                int ind8 = strLine.lastIndexOf("ОПЕРАЦИЯ  ");
                                String doc8 = System.lineSeparator() + strLine.substring(ind7, ind8);

                                int ind9 = strLine.lastIndexOf("ВАЛЮТА  ");
                                String doc9 = System.lineSeparator() + strLine.substring(ind8, ind9);

                                int ind10 = strLine.lastIndexOf("НОМЕР     :");
                                String doc10 = System.lineSeparator() + strLine.substring(ind9, ind10);

                                int ind11 = strLine.lastIndexOf("СТАДИЯДОК ");
                                String doc11 = System.lineSeparator() + strLine.substring(ind10, ind11);

                                int ind12 = strLine.lastIndexOf("ДЕБЕТ ");
                                String doc12 = System.lineSeparator() + strLine.substring(ind11, ind12);

                                int ind13 = strLine.lastIndexOf("КРЕДИТ    ");
                                String doc13 = System.lineSeparator() + strLine.substring(ind12, ind13);

                                int ind14 = strLine.lastIndexOf("СУММА  ");
                                String doc14 = System.lineSeparator() + strLine.substring(ind13, ind14);

                                int ind15 = strLine.lastIndexOf("ДАТАПЛАТ ");
                                String doc15 = System.lineSeparator() + strLine.substring(ind14, ind15);

                                int ind16 = strLine.lastIndexOf("ОЧЕРЕДНПЛ ");
                                String doc16 = System.lineSeparator() + strLine.substring(ind15, ind16);

                                int ind17 = strLine.lastIndexOf("ГРУППАДОК ");
                                String doc17 = System.lineSeparator() + strLine.substring(ind16, ind17);

                                int ind18 = strLine.lastIndexOf("МФООТПР ");
                                String doc18 = System.lineSeparator() + strLine.substring(ind17, ind18);

                                int ind19 = strLine.lastIndexOf("Р/СОТПР ");
                                String doc19 = System.lineSeparator() + strLine.substring(ind18, ind19);

                                int ind20 = strLine.lastIndexOf("ИМЯОТПР ");
                                String doc20 = System.lineSeparator() + strLine.substring(ind19, ind20);

                                int ind21 = strLine.lastIndexOf("ИННОТПР ");
                                String doc21 = System.lineSeparator() + strLine.substring(ind20, ind21);

                                int ind22 = strLine.lastIndexOf("МФОПОЛУЧ ");
                                String doc22 = System.lineSeparator() + strLine.substring(ind21, ind22);

                                int ind23 = strLine.lastIndexOf("Р/СПОЛУЧ ");
                                String doc23 = System.lineSeparator() + strLine.substring(ind22, ind23);

                                int ind24 = strLine.lastIndexOf("ИМЯПОЛУЧ ");
                                String doc24 = System.lineSeparator() + strLine.substring(ind23, ind24);

                                int ind25 = strLine.lastIndexOf("ИННПОЛУЧ");
                                String doc25 = System.lineSeparator() + strLine.substring(ind24, ind25);

                                int ind26 = strLine.lastIndexOf("ПРИМ1  ");
                                String doc26 = System.lineSeparator() + strLine.substring(ind25, ind26);

                                int ind27 = strLine.lastIndexOf("ПРИМ2 ");
                                String doc27 = System.lineSeparator() + strLine.substring(ind26, ind27);

                                int ind28 = strLine.lastIndexOf("ПРИМ3 ");
                                String doc28 = System.lineSeparator() + strLine.substring(ind27, ind28);

                                int ind29 = strLine.lastIndexOf("ПРИМ3 ");
                                String doc29 = "";

                                int ind30 = strLine.lastIndexOf("ПРИМ4 ");
                                String doc30 = System.lineSeparator() + strLine.substring(ind29, ind30);

                                int ind31 = strLine.lastIndexOf("ПРИМ5 ");
                                String doc31 = System.lineSeparator() + strLine.substring(ind30, ind31);

                                int ind32 = strLine.lastIndexOf("ПРИМ6 ");
                                String doc32 = System.lineSeparator() + strLine.substring(ind31, ind32);

                                int ind33 = strLine.lastIndexOf("ПРИМ7 ");
                                String doc33 = System.lineSeparator() + strLine.substring(ind32, ind33);

                                int ind34 = strLine.lastIndexOf("ПРИМ8 ");
                                String doc34 = System.lineSeparator() + strLine.substring(ind33, ind34);

                                int ind35 = strLine.lastIndexOf("ПРИМ9 ");
                                String doc35 = System.lineSeparator() + strLine.substring(ind34, ind35);

                                int ind36 = strLine.lastIndexOf("ПРИМ10 ");
                                String doc36 = System.lineSeparator() + strLine.substring(ind35, ind36);
                                int ind37 = strLine.lastIndexOf("ПРИМ11 ");
                                String doc37 = System.lineSeparator() + strLine.substring(ind36, ind37);
                                int ind38 = strLine.lastIndexOf("ПРИМ12 ");
                                String doc38 = System.lineSeparator() + strLine.substring(ind37, ind38);
                                int ind39 = strLine.lastIndexOf("ПРИМ13 ");
                                String doc39 = System.lineSeparator() + strLine.substring(ind38, ind39);
                                int ind40 = strLine.lastIndexOf("ПРИМ14 ");
                                String doc40 = System.lineSeparator() + strLine.substring(ind39, ind40);
                                int ind41 = strLine.lastIndexOf("ПРИМ15");
                                String doc41 = System.lineSeparator() + strLine.substring(ind40, ind41);
                                int ind42 = strLine.lastIndexOf("ПРИМ16 ");
                                String doc42 = System.lineSeparator() + strLine.substring(ind41, ind42);
                                int ind43 = strLine.lastIndexOf("БАНКПОЛУЧ ");
                                String doc43 = System.lineSeparator() + strLine.substring(ind42, ind43);
                                int ind44 = strLine.lastIndexOf("БАНКОТПР ");
                                String doc44 = System.lineSeparator() + strLine.substring(ind43, ind44);
                                int ind45 = strLine.lastIndexOf("КПППЛАТ");
                                String doc45 = System.lineSeparator() + strLine.substring(ind44, ind45);
                                int ind46 = strLine.lastIndexOf("КПППОЛ");
                                String doc46 = System.lineSeparator() + strLine.substring(ind45, ind46);
                                int ind47 = strLine.lastIndexOf("СТАТПЛАТ");
                                String doc47 = System.lineSeparator() + strLine.substring(ind46, ind47);
                                int ind48 = strLine.lastIndexOf("КБК");
                                String doc48 = System.lineSeparator() + strLine.substring(ind47, ind48);
                                int ind49 = strLine.lastIndexOf("ОКАТО");
                                String doc49 = System.lineSeparator() + strLine.substring(ind48, ind49);
                                int ind50 = strLine.lastIndexOf("ОСНПЛАТ");
                                String doc50 = System.lineSeparator() + strLine.substring(ind49, ind50);
                                int ind51 = strLine.lastIndexOf("КОДТО ");
                                String doc51 = System.lineSeparator() + strLine.substring(ind50, ind51);
                                int ind52 = strLine.lastIndexOf("НОМЕРТД ");
                                String doc52 = System.lineSeparator() + strLine.substring(ind51, ind52);
                                int ind53 = strLine.lastIndexOf("ДАТАТД ");
                                String doc53 = System.lineSeparator() + strLine.substring(ind52, ind53);
                                int ind54 = strLine.lastIndexOf("ТИПТП ");
                                String doc54 = System.lineSeparator() + strLine.substring(ind53, ind54);
                                int ind55 = strLine.lastIndexOf("ДАТАСП ");
                                String doc55 = System.lineSeparator() + strLine.substring(ind54, ind55);
                                int ind56 = strLine.lastIndexOf("УИН ");
                                String doc56 = System.lineSeparator() + strLine.substring(ind55, ind56);
                                int ind57 = strLine.lastIndexOf("%%МОПК1 ");
                                String doc57 = System.lineSeparator() + strLine.substring(ind56, ind57);
                                int ind58 = strLine.lastIndexOf("%%ID_ERP");
                                String doc58 = System.lineSeparator() + strLine.substring(ind57, ind58);
                                int ind59 = strLine.lastIndexOf("%END");
                                String doc59 = System.lineSeparator() + strLine.substring(ind58, ind59);

                                String ress;                              
                                ress = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:onec=\"http://OneCFlowsLibrary/ru/neoflex/intesa/onecflows/intf/OneCInboundInterface\">\n"
                                        + "   <soapenv:Header/>\n"
                                        + "   <soapenv:Body>\n"
                                        + "      <onec:loadDocument>\n"
                                        + "         <requestId>" + document + "</requestId>\n"
                                        + "         <body>" + doc2 + doc3 + doc4 + doc5 + doc6 + doc7 + doc8 + doc9 + doc10
                                        + doc11 + doc12 + doc13 + doc14 + doc15 + doc16 + doc17 + doc18 + doc19
                                        + doc20 + doc21 + doc22 + doc23 + doc24 + doc25 + doc26 + doc27 + doc28 + doc29
                                        + doc30 + doc31 + doc32 + doc33 + doc34 + doc35 + doc36 + doc37 + doc38 + doc39
                                        + doc40 + doc41 + doc42 + doc43 + doc44 + doc45 + doc46 + doc47 + doc48 + doc49
                                        + doc50 + doc51 + doc52 + doc53 + doc54 + doc55 + doc56 + doc57 + doc58 + doc59
                                        + System.lineSeparator() + "%END"
                                        + "</body>\n"
                                        + "      </onec:loadDocument>\n"
                                        + "   </soapenv:Body>\n"
                                        + "</soapenv:Envelope>";
                                
                                                            
                                wrstream.write(ress.getBytes("UTF-8"), 0, ress.getBytes("UTF-8").length);
                                System.out.println("OK");
                               // fstream.close();
                                File file = new File(dir.getPath() + "/" + item.getName());
                                if (file.delete()) {
                                    System.out.println(dir.getPath() + "/" + item.getName() +" cleared");
                                } else {
                                    System.out.println(dir.getPath() + "/" + item.getName() + " file not cleared");
                                }

                            }

                            if (strLine.lastIndexOf("nalog") > 0) {
                               
                                System.out.println(item.getName());
                                System.out.println("nalog... ");

                                int ind1 = strLine.lastIndexOf("%МЕЖБНКДОК");
                                String document = strLine.substring(12, ind1 - 8);

                                int ind2 = strLine.lastIndexOf("nalog");
                                String doc2 = strLine.substring(ind1, ind2);

                                int ind3 = strLine.lastIndexOf("ДАТА ");
                                String doc3 = System.lineSeparator() + strLine.substring(ind2, ind3);

                                int ind4 = strLine.lastIndexOf("ПЛАН ");
                                String doc4 = System.lineSeparator() + strLine.substring(ind3, ind4);

                                int ind5 = strLine.lastIndexOf("ФИЛИАЛ  ");
                                String doc5 = System.lineSeparator() + strLine.substring(ind4, ind5);

                                int ind6 = strLine.lastIndexOf("ОБЛАСТЬ   :");
                                String doc6 = System.lineSeparator() + strLine.substring(ind5, ind6);

                                int ind7 = strLine.lastIndexOf("ПАЧКА     :");
                                String doc7 = System.lineSeparator() + strLine.substring(ind6, ind7);

                                int ind8 = strLine.lastIndexOf("ОПЕРАЦИЯ  :");
                                String doc8 = System.lineSeparator() + strLine.substring(ind7, ind8);

                                int ind9 = strLine.lastIndexOf("ВАЛЮТА    :");
                                String doc9 = System.lineSeparator() + strLine.substring(ind8, ind9);

                                int ind10 = strLine.lastIndexOf("НОМЕР     :");
                                String doc10 = System.lineSeparator() + strLine.substring(ind9, ind10);

                                int ind11 = strLine.lastIndexOf("СТАДИЯДОК :");
                                String doc11 = System.lineSeparator() + strLine.substring(ind10, ind11);

                                int ind12 = strLine.lastIndexOf("ДЕБЕТ     :");
                                String doc12 = System.lineSeparator() + strLine.substring(ind11, ind12);

                                int ind13 = strLine.lastIndexOf("КРЕДИТ    :");
                                String doc13 = System.lineSeparator() + strLine.substring(ind12, ind13);

                                int ind14 = strLine.lastIndexOf("СУММА     :");
                                String doc14 = System.lineSeparator() + strLine.substring(ind13, ind14);

                                int ind15 = strLine.lastIndexOf("ДАТАПЛАТ  :");
                                String doc15 = System.lineSeparator() + strLine.substring(ind14, ind15);

                                int ind16 = strLine.lastIndexOf("ОЧЕРЕДНПЛ :");
                                String doc16 = System.lineSeparator() + strLine.substring(ind15, ind16);

                                int ind17 = strLine.lastIndexOf("ГРУППАДОК :");
                                String doc17 = System.lineSeparator() + strLine.substring(ind16, ind17);

                                int ind18 = strLine.lastIndexOf("МФООТПР   :");
                                String doc18 = System.lineSeparator() + strLine.substring(ind17, ind18);

                                int ind19 = strLine.lastIndexOf("Р/СОТПР   :");
                                String doc19 = System.lineSeparator() + strLine.substring(ind18, ind19);

                                int ind20 = strLine.lastIndexOf("ИМЯОТПР   :");
                                String doc20 = System.lineSeparator() + strLine.substring(ind19, ind20);

                                int ind21 = strLine.lastIndexOf("ИННОТПР   :");
                                String doc21 = System.lineSeparator() + strLine.substring(ind20, ind21);

                                int ind22 = strLine.lastIndexOf("МФОПОЛУЧ  :");
                                String doc22 = System.lineSeparator() + strLine.substring(ind21, ind22);

                                int ind23 = strLine.lastIndexOf("Р/СПОЛУЧ  :");
                                String doc23 = System.lineSeparator() + strLine.substring(ind22, ind23);

                                int ind24 = strLine.lastIndexOf("ИМЯПОЛУЧ  :");
                                String doc24 = System.lineSeparator() + strLine.substring(ind23, ind24);

                                int ind25 = strLine.lastIndexOf("ИННПОЛУЧ  :");
                                String doc25 = System.lineSeparator() + strLine.substring(ind24, ind25);

                                int ind26 = strLine.lastIndexOf("ПРИМ1     :");
                                String doc26 = System.lineSeparator() + strLine.substring(ind25, ind26);

                                int ind27 = strLine.lastIndexOf("ПРИМ2     :");
                                String doc27 = System.lineSeparator() + strLine.substring(ind26, ind27);

                                int ind28 = strLine.lastIndexOf("ПРИМ3     :");
                                String doc28 = System.lineSeparator() + strLine.substring(ind27, ind28);

                                int ind29 = strLine.lastIndexOf("ПРИМ4     :");
                                String doc29 = System.lineSeparator() + strLine.substring(ind28, ind29);

                                int ind30 = strLine.lastIndexOf("ПРИМ5     :");
                                String doc30 = System.lineSeparator() + strLine.substring(ind29, ind30);

                                int ind31 = strLine.lastIndexOf("ПРИМ6     :");
                                String doc31 = System.lineSeparator() + strLine.substring(ind30, ind31);

                                int ind32 = strLine.lastIndexOf("ПРИМ7     :");
                                String doc32 = System.lineSeparator() + strLine.substring(ind31, ind32);

                                int ind33 = strLine.lastIndexOf("ПРИМ8     :");
                                String doc33 = System.lineSeparator() + strLine.substring(ind32, ind33);

                                int ind34 = strLine.lastIndexOf("ПРИМ9     :");
                                String doc34 = System.lineSeparator() + strLine.substring(ind33, ind34);

                                int ind35 = strLine.lastIndexOf("ПРИМ10    :");
                                String doc35 = System.lineSeparator() + strLine.substring(ind34, ind35);

                                int ind36 = strLine.lastIndexOf("ПРИМ11    :");
                                String doc36 = System.lineSeparator() + strLine.substring(ind35, ind36);
                                int ind37 = strLine.lastIndexOf("ПРИМ12    :");
                                String doc37 = System.lineSeparator() + strLine.substring(ind36, ind37);
                                int ind38 = strLine.lastIndexOf("ПРИМ13    :");
                                String doc38 = System.lineSeparator() + strLine.substring(ind37, ind38);
                                int ind39 = strLine.lastIndexOf("ПРИМ14    :");
                                String doc39 = System.lineSeparator() + strLine.substring(ind38, ind39);
                                int ind40 = strLine.lastIndexOf("ПРИМ15    :");
                                String doc40 = System.lineSeparator() + strLine.substring(ind39, ind40);

                                int ind41 = strLine.lastIndexOf("ПРИМ16    :");
                                String doc41 = System.lineSeparator() + strLine.substring(ind40, ind41);

                                int ind42 = strLine.lastIndexOf("ПОЛУЧ :");
                                String doc42 = System.lineSeparator() + strLine.substring(ind41, ind42);
                                int ind43 = strLine.lastIndexOf("ОТПР  :");
                                String doc43 = System.lineSeparator() + strLine.substring(ind42, ind43);

                                int ind44 = strLine.lastIndexOf("КПППЛАТ   :");
                                String doc44 = System.lineSeparator() + strLine.substring(ind43, ind44);

                                int ind45 = strLine.lastIndexOf("КПППОЛ    :");
                                String doc45 = System.lineSeparator() + strLine.substring(ind44, ind45);

                                int ind46 = strLine.lastIndexOf("СТАТПЛАТ  :");
                                String doc46 = System.lineSeparator() + strLine.substring(ind45, ind46);

                                int ind47 = strLine.lastIndexOf("ТИПНП     :");
                                String doc47 = System.lineSeparator() + strLine.substring(ind46, ind47);

                                int ind48 = strLine.lastIndexOf("КБК       :");
                                String doc48 = System.lineSeparator() + strLine.substring(ind47, ind48);

                                int ind49 = strLine.lastIndexOf("ОКАТО     :");
                                String doc49 = System.lineSeparator() + strLine.substring(ind48, ind49);

                                int ind50 = strLine.lastIndexOf("ОСНПЛАТ   :");
                                String doc50 = System.lineSeparator() + strLine.substring(ind49, ind50);
                                int ind51 = strLine.lastIndexOf("НАЛПРД    :");
                                String doc51 = System.lineSeparator() + strLine.substring(ind50, ind51);
                                int ind52 = strLine.lastIndexOf("НОМЕРНД   :");
                                String doc52 = System.lineSeparator() + strLine.substring(ind51, ind52);
                                int ind53 = strLine.lastIndexOf("ДАТАНД    :");
                                String doc53 = System.lineSeparator() + strLine.substring(ind52, ind53);
                                int ind54 = strLine.lastIndexOf("ДАТАСП    :");
                                String doc54 = System.lineSeparator() + strLine.substring(ind53, ind54);
                                int ind55 = strLine.lastIndexOf("УИН       :");
                                String doc55 = System.lineSeparator() + strLine.substring(ind54, ind55);
                                int ind56 = strLine.lastIndexOf("%%МОПК1   :");
                                String doc56 = System.lineSeparator() + strLine.substring(ind55, ind56);
                                int ind57 = strLine.lastIndexOf("%%ID_ER   :");
                                String doc57 = System.lineSeparator() + strLine.substring(ind56, ind57);
                                int ind58 = strLine.lastIndexOf("%END");
                                String doc58 = System.lineSeparator() + strLine.substring(ind57, ind58);

                                String ress;
                                ress = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:onec=\"http://OneCFlowsLibrary/ru/neoflex/intesa/onecflows/intf/OneCInboundInterface\">\n"
                                        + "   <soapenv:Header/>\n"
                                        + "   <soapenv:Body>\n"
                                        + "      <onec:loadDocument>\n"
                                        + "         <requestId>" + document + "</requestId>\n"
                                        + "         <body>" + doc2 + doc3 + doc4 + doc5 + doc6 + doc7 + doc8 + doc9 + doc10
                                        + doc11 + doc12 + doc13 + doc14 + doc15 + doc16 + doc17 + doc18 + doc19
                                        + doc20 + doc21 + doc22 + doc23 + doc24 + doc25 + doc26 + doc27 + doc28 + doc29
                                        + doc30 + doc31 + doc32 + doc33 + doc34 + doc35 + doc36 + doc37 + doc38 + doc39
                                        + doc40 + doc41 + doc42 + doc43 + doc44 + doc45 + doc46 + doc47 + doc48 + doc49
                                        + doc50 + doc51 + doc52 + doc53 + doc54 + doc55 + doc56 + doc57 + doc58
                                        + System.lineSeparator() + "%END"
                                        + "</body>\n"
                                        + "      </onec:loadDocument>\n"
                                        + "   </soapenv:Body>\n"
                                        + "</soapenv:Envelope>";

                                System.out.println("***************");
                                //  System.out.println(ress);

                                wrstream.write(ress.getBytes("UTF-8"), 0, ress.getBytes("UTF-8").length);
                                System.out.println("OK");
                               
                              
                             //   fstream.close();
                                File file = new File(dir.getPath() + "/" + item.getName());
                                if (file.delete()) {
                                    System.out.println(dir.getPath() + "/" + item.getName() +" cleared");
                                } else {
                                    System.out.println(dir.getPath() + "/" + item.getName() + " file not cleared");
                                }

                            }

                            if (strLine.lastIndexOf("mezhfil") > 0) {
                                
                                System.out.println(item.getName());
                                System.out.print("mezhfil... ");

                                int ind1 = strLine.lastIndexOf("%МЕЖБНКДОК");
                                String document = strLine.substring(12, ind1 - 8);

                                int ind2 = strLine.lastIndexOf("mezhfil");
                                String doc2 = strLine.substring(ind1, ind2);

                                int ind3 = strLine.lastIndexOf("ДАТА      :");
                                String doc3 = System.lineSeparator() + strLine.substring(ind2, ind3);

                                int ind4 = strLine.lastIndexOf("ПЛАН      :");
                                String doc4 = System.lineSeparator() + strLine.substring(ind3, ind4);

                                int ind5 = strLine.lastIndexOf("ФИЛИАЛ    :");
                                String doc5 = System.lineSeparator() + strLine.substring(ind4, ind5);

                                int ind6 = strLine.lastIndexOf("ОБЛАСТЬ   :");
                                String doc6 = System.lineSeparator() + strLine.substring(ind5, ind6);

                                int ind7 = strLine.lastIndexOf("ВАЛЮТА    :");
                                String doc7 = System.lineSeparator() + strLine.substring(ind6, ind7);

                                int ind8 = strLine.lastIndexOf("ОПЕРАЦИЯ  :");
                                String doc8 = System.lineSeparator() + strLine.substring(ind7, ind8);

                                int ind9 = strLine.lastIndexOf("НОМЕР     :");
                                String doc9 = System.lineSeparator() + strLine.substring(ind8, ind9);

                                int ind10 = strLine.lastIndexOf("СТАДИЯДОК :");
                                String doc10 = System.lineSeparator() + strLine.substring(ind9, ind10);

                                int ind11 = strLine.lastIndexOf("СУММА     :");
                                String doc11 = System.lineSeparator() + strLine.substring(ind10, ind11);

                                int ind12 = strLine.lastIndexOf("ДАТАПЛАТ  :");
                                String doc12 = System.lineSeparator() + strLine.substring(ind11, ind12);

                                int ind13 = strLine.lastIndexOf("ДЕБЕТ     :");
                                String doc13 = System.lineSeparator() + strLine.substring(ind12, ind13);

                                int ind14 = strLine.lastIndexOf("КРЕДИТ    :");
                                String doc14 = System.lineSeparator() + strLine.substring(ind13, ind14);

                                int ind15 = strLine.lastIndexOf("ОЧЕРЕДНПЛ :");
                                String doc15 = System.lineSeparator() + strLine.substring(ind14, ind15);

                                int ind16 = strLine.lastIndexOf("МФООТПР   :");
                                String doc16 = System.lineSeparator() + strLine.substring(ind15, ind16);

                                int ind17 = strLine.lastIndexOf("Р/СОТПР   :");
                                String doc17 = System.lineSeparator() + strLine.substring(ind16, ind17);

                                int ind18 = strLine.lastIndexOf("ИМЯОТПР   :");
                                String doc18 = System.lineSeparator() + strLine.substring(ind17, ind18);

                                int ind19 = strLine.lastIndexOf("МФОПОЛУЧ  :");
                                String doc19 = System.lineSeparator() + strLine.substring(ind18, ind19);

                                int ind20 = strLine.lastIndexOf("Р/СПОЛУЧ  :");
                                String doc20 = System.lineSeparator() + strLine.substring(ind19, ind20);

                                int ind21 = strLine.lastIndexOf("ИМЯПОЛУЧ  :");
                                String doc21 = System.lineSeparator() + strLine.substring(ind20, ind21);

                                int ind22 = strLine.lastIndexOf("ДАТАПРДОК :");
                                String doc22 = System.lineSeparator() + strLine.substring(ind21, ind22);

                                int ind23 = strLine.lastIndexOf("ИННОТПР   :");
                                String doc23 = System.lineSeparator() + strLine.substring(ind22, ind23);

                                int ind24 = strLine.lastIndexOf("ИННПОЛУЧ  :");
                                String doc24 = System.lineSeparator() + strLine.substring(ind23, ind24);

                                int ind25 = strLine.lastIndexOf("ИННПОЛУЧ  :");
                                String doc25 = System.lineSeparator() + strLine.substring(ind24, ind25);

                                int ind26 = strLine.lastIndexOf("КПППЛАТ   :");
                                String doc26 = System.lineSeparator() + strLine.substring(ind25, ind26);

                                int ind27 = strLine.lastIndexOf("КПППОЛ    :");
                                String doc27 = System.lineSeparator() + strLine.substring(ind26, ind27);

                                int ind28 = strLine.lastIndexOf("ДАТАСП    :");
                                String doc28 = System.lineSeparator() + strLine.substring(ind27, ind28);

                                int ind29 = strLine.lastIndexOf("ПРИМ1     :");
                                String doc29 = System.lineSeparator() + strLine.substring(ind28, ind29);

                                int ind30 = strLine.lastIndexOf("ПРИМ2     :");
                                String doc30 = System.lineSeparator() + strLine.substring(ind29, ind30);

                                int ind31 = strLine.lastIndexOf("ПРИМ3     :");
                                String doc31 = System.lineSeparator() + strLine.substring(ind30, ind31);

                                int ind32 = strLine.lastIndexOf("ПРИМ4     :");
                                String doc32 = System.lineSeparator() + strLine.substring(ind31, ind32);

                                int ind33 = strLine.lastIndexOf("ПРИМ5     :");
                                String doc33 = System.lineSeparator() + strLine.substring(ind32, ind33);

                                int ind34 = strLine.lastIndexOf("ПРИМ6     :");
                                String doc34 = System.lineSeparator() + strLine.substring(ind33, ind34);

                                int ind35 = strLine.lastIndexOf("ПРИМ7     :");
                                String doc35 = System.lineSeparator() + strLine.substring(ind34, ind35);

                                int ind36 = strLine.lastIndexOf("ПРИМ8     :");
                                String doc36 = System.lineSeparator() + strLine.substring(ind35, ind36);
                                int ind37 = strLine.lastIndexOf("ПРИМ9     :");
                                String doc37 = System.lineSeparator() + strLine.substring(ind36, ind37);
                                int ind38 = strLine.lastIndexOf("ПРИМ10    :");
                                String doc38 = System.lineSeparator() + strLine.substring(ind37, ind38);
                                int ind39 = strLine.lastIndexOf("ПРИМ11    :");
                                String doc39 = System.lineSeparator() + strLine.substring(ind38, ind39);
                                int ind40 = strLine.lastIndexOf("ПРИМ12    :");
                                String doc40 = System.lineSeparator() + strLine.substring(ind39, ind40);

                                int ind41 = strLine.lastIndexOf("ПРИМ13    :");
                                String doc41 = System.lineSeparator() + strLine.substring(ind40, ind41);

                                int ind42 = strLine.lastIndexOf("ПРИМ14    :");
                                String doc42 = System.lineSeparator() + strLine.substring(ind41, ind42);
                                int ind43 = strLine.lastIndexOf("ПРИМ15    :");
                                String doc43 = System.lineSeparator() + strLine.substring(ind42, ind43);

                                int ind44 = strLine.lastIndexOf("ПРИМ16    :");
                                String doc44 = System.lineSeparator() + strLine.substring(ind43, ind44);

                                int ind45 = strLine.lastIndexOf("СТАТПЛАТ  :");
                                String doc45 = System.lineSeparator() + strLine.substring(ind44, ind45);

                                int ind46 = strLine.lastIndexOf("КБК       :");
                                String doc46 = System.lineSeparator() + strLine.substring(ind45, ind46);

                                int ind47 = strLine.lastIndexOf("ОКАТО     :");
                                String doc47 = System.lineSeparator() + strLine.substring(ind46, ind47);

                                int ind48 = strLine.lastIndexOf("ОСНПЛАТ   :");
                                String doc48 = System.lineSeparator() + strLine.substring(ind47, ind48);

                                int ind49 = strLine.lastIndexOf("КОДТО     :");
                                String doc49 = System.lineSeparator() + strLine.substring(ind48, ind49);

                                int ind50 = strLine.lastIndexOf("НОМЕРТД   :");
                                String doc50 = System.lineSeparator() + strLine.substring(ind49, ind50);

                                int ind51 = strLine.lastIndexOf("ДАТАТД    :");
                                String doc51 = System.lineSeparator() + strLine.substring(ind50, ind51);

                                int ind52 = strLine.lastIndexOf("ТИПТП     :");
                                String doc52 = System.lineSeparator() + strLine.substring(ind51, ind52);

                                int ind53 = strLine.lastIndexOf("ПАЧКА     :");
                                String doc53 = System.lineSeparator() + strLine.substring(ind52, ind53);

                                int ind54 = strLine.lastIndexOf("УИН       :");
                                String doc54 = System.lineSeparator() + strLine.substring(ind53, ind54);
                                int ind55 = strLine.lastIndexOf("%%МОПК1   :");
                                String doc55 = System.lineSeparator() + strLine.substring(ind54, ind55);
                                int ind56 = strLine.lastIndexOf("%%ID_ERP  :");
                                String doc56 = System.lineSeparator() + strLine.substring(ind55, ind56);
                                int ind57 = strLine.lastIndexOf("%END");
                                String doc57 = System.lineSeparator() + strLine.substring(ind56, ind57);

                                String ress;
                                ress = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:onec=\"http://OneCFlowsLibrary/ru/neoflex/intesa/onecflows/intf/OneCInboundInterface\">\n"
                                        + "   <soapenv:Header/>\n"
                                        + "   <soapenv:Body>\n"
                                        + "      <onec:loadDocument>\n"
                                        + "         <requestId>" + document + "</requestId>\n"
                                        + "         <body>" + doc2 + doc3 + doc4 + doc5 + doc6 + doc7 + doc8 + doc9 + doc10
                                        + doc11 + doc12 + doc13 + doc14 + doc15 + doc16 + doc17 + doc18 + doc19
                                        + doc20 + doc21 + doc22 + doc23 + doc24 + doc25 + doc26 + doc27 + doc28 + doc29
                                        + doc30 + doc31 + doc32 + doc33 + doc34 + doc35 + doc36 + doc37 + doc38 + doc39
                                        + doc40 + doc41 + doc42 + doc43 + doc44 + doc45 + doc46 + doc47 + doc48 + doc49
                                        + doc50 + doc51 + doc52 + doc53 + doc54 + doc55 + doc56 + doc57
                                        + System.lineSeparator() + "%END"
                                        + "</body>\n"
                                        + "      </onec:loadDocument>\n"
                                        + "   </soapenv:Body>\n"
                                        + "</soapenv:Envelope>";

                                System.out.println(ress);
                                
                                wrstream.write(ress.getBytes("UTF-8"), 0, ress.getBytes("UTF-8").length);
                                System.out.println("OK");
                                
                                //fstream.close();
                                File file = new File(dir.getPath() + "/" + item.getName());
                                if (file.delete()) {
                                    System.out.println(dir.getPath() + "/" + item.getName() +" cleared");
                                } else {
                                    System.out.println(dir.getPath() + "/" + item.getName() + " file not cleared");
                                }

                            }

                        }

                    } catch (IOException e) {
                        System.out.println("Error");
                    }

                }
            }
        }
    }
}
